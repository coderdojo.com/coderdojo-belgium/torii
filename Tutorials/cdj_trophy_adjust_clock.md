# CDJ Trophy: Adjusting the clock

Congratz, You’re a proud owner of the cool Coderdojo Trophy!

[![CDJ Trophy](https://img.youtube.com/vi/olLxNKEY9lY/0.jpg)](https://youtu.be/olLxNKEY9lY)

## Would you like to correct the RGB-hands of the clock?

1. Connect the trophy via a USB adaptor to the mains.

2. Use a Wifi device (eg. Smartphone) to connect to the  ‘Coderdojo Trofee’ network.
(You will be disconnected from the internet).

3. Open a browser and type IP-address `192.168.4.1`. [Setup Page](images/setuppage.png)

4. Fill in Hour and minutes and click `Submit`.

5. Your mobile device can be disconnected from the `Coderdojo Trofee` network and reconnected to your normal Wifi connection.

**Done!**

## Ter info

Our trophy’s brain is setup around an **ESP8266**.  This microcontroller can easily be coded via the Arduino IDE.  Here’s a link where you can download it from: <https://www.arduino.cc/en/software>
![Arduino IDE](images/arduino.png)

Use these instructions to connect the ESP8266 to your Arduino IDE <https://arduino-esp8266.readthedocs.io/en/latest/installing.html>

![ESP8266](images/esp8266.png)

You would like to fiddle around with our code?  Have a go via this url: <https://gitlab.com/coderdojo.com/coderdojo-belgium/torii>

![Torii](images/torii.png)
