# CDJ Trofee: De klok juist zetten

Je bent nu de trotse bezitter van een coole Coderdojo Trofee.

[![CDJ Trophy](https://img.youtube.com/vi/olLxNKEY9lY/0.jpg)](https://youtu.be/olLxNKEY9lY)

## Wil je graag de ingebouwde klok instellen?

1. Zorg ervoor dat de trofee ingeplugd is en de ledjes oplichten.

2. Neem je gsm bij de hand en verbind met het `Coderdojo Trofee` netwerk.
(Je hebt dan tijdelijk geen internet verbinding.  Op die manier wordt er geen onveilige poort geopend).

3. Open een browser en type het IP-adres `192.168.4.1`. [Setup Page](images/setuppage.png)

4. Geef het huidige uur en minuut in en klik op `Submit`.

5. Je kan je gsm terug verbinden met het wifi-netwerk dat jij normaal gebruikt, om opnieuw te kunnen surfen op ‘t net.

**Done!**

## Ter info

Het brein achter onze trofee is een **ESP8266**.  Deze kan je makkelijk (her)programmeren via de Arduino IDE. Die kan je downloaden via <https://www.arduino.cc/en/software>
![Arduino IDE](images/arduino.png)

Volg deze instructies om de ESP8266 bekend te maken in de Arduino IDE: <https://arduino-esp8266.readthedocs.io/en/latest/installing.html>
![ESP8266](images/esp8266.png)

Als je onze zelfgemaakte code wil inspecteren, of ze opnieuw flashen op de trophy, die vind je op deze url: <https://gitlab.com/coderdojo.com/coderdojo-belgium/torii>
![Torii](images/torii.png)