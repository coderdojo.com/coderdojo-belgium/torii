#include <TimeLib.h>
#include <ESP8266WiFi.h>

// Set web server port number to 80
WiFiServer server(80);
// Variable to store the HTTP request
String header;

void StartWebServer()
{

  Serial.println(__TIME__);
  WiFi.softAP("CoderDojo Trophy");
  //WiFi.softAP(ssid, psk, channel, hidden, max_connection)
  //softAPConfig (local_ip, gateway, subnet);
  Serial.print("Soft-AP IP address = ");
  Serial.println(WiFi.softAPIP()); //default address for an access point is http://192.168.4.1/

  
  server.begin();
}

void WritePage(Stream *s) {
  s->println("HTTP/1.1 200 OK");
  s->println("Content-type:text/html");
  s->println("Connection: close");
  s->println();
  s->println("<!DOCTYPE html>");
  s->println("<html><head><title>CoderDojo Trophy Setup Page</title></head>");
  s->println("<h1>CoderDojo Trophy Setup Page</h1>");
  s->println("<form action=\"/time\" method=\"get\">");
  s->println("  <label for=\"hour\">Hour:</label>");
  s->println("  <input type=\"number\" id=\"hour\" name=\"hour\" min=\"0\" max=\"23\"><br>");
  s->println("  <label for=\"minutes\">minutes:</label>");
  s->println("  <input type=\"number\" id=\"minutes\" name=\"minutes\" min=\"0\" max=\"59\"><br><br>");
  s->println("  <input type=\"submit\" value=\"Submit\">");
  s->println("</form>");
}

int extractParameter(String header, String param) {
  int loc = header.indexOf(param);
  if (loc < 0) return -1;

  loc += param.length(); //we start looking at the location past the parameter
  int value = 0;
  while (isDigit(header.charAt(loc))) {
    int delta = header.charAt(loc) - '0'; 
    value *= 10; //first, shift previous digits
    value += header.charAt(loc) - '0';
    //Serial.print("digit="); Serial.print(header.charAt(loc));
    //Serial.print(", delta="); Serial.print(delta);
    //Serial.print(", value="); Serial.println(value);
    //Serial.flush();
    loc++;
  }

  Serial.print("param \"");
  Serial.print(param);
  Serial.print("\" = ");
  Serial.println(value);

  return value;
}

void SetTimeIfGiven(String header)
{
  int h = extractParameter(header, "hour=");
  int m = extractParameter(header, "minutes=");
  if (h > -1 && m > -1) {
    setTime(h, m, 0, 3, 3, 2023);
    Serial.print("Time was set to ");
    Serial.print(hour());
    Serial.print(":");
    Serial.print(minute());
    Serial.println();
  }
}

void ServeIncomingHTTPConnection() 
{
  WiFiClient client = server.available();   // Listen for incoming clients

  if (!client) return;
  
  Serial.println("New Client.");          // print a message out in the serial port
  Serial.flush();
  client.setTimeout(1000);  // default is 1000

  header = client.readString();
  if (header.indexOf("favicon.ico") < 0) {
    //ignore requests for favicon to keep the log clutter-free:
    Serial.println(F("request: "));
    Serial.println(header);
  }

  SetTimeIfGiven(header);

  WritePage(&client);
}//ServeIncomingHTTPConnection

