/*
 * Hardware connections:
 * 
 *    ESP8266        --> MPU9265
 *    D5 (GPI14|SCL) --> SCL
 *    D4 (GPIO2|SDA) --> SDA
 *    GND            --> GND
 *    3V3            --> VCC
 *    
 *    
 *    ESP8266           --> LED strip
 *    D7 (GPI15|TXD1)   --> Data
 *    GND               --> GND
 *    Vin (5V from USB) --> VCC
 *
 *
 *    ESP8266         --> Speaker
 *    D8 (GPI13)      --> SpkIn
 *    GND             --> GND
 *    VCC (5V)        --> VCC
 */

#include <Adafruit_MPU6050.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_NeoPixel.h>

#include <TimeLib.h>

#define SPEAKER_PIN 13

#define LED_PIN 15       // Pin for the LED strip
#define NUM_LEDS 24      // Number of LEDs in the strip
#define BRIGHTNESS 128  // Brightness of the LEDs (0-255)

Adafruit_MPU6050 mpu;
Adafruit_NeoPixel pixels(NUM_LEDS, LED_PIN, NEO_GRB + NEO_KHZ800);

#define SLOW_SHIFT 10
#define SWING_SHIFT 1000
#define SWINGTIME_MILLIS 1000

void setup()
{
  pinMode(SPEAKER_PIN, OUTPUT);
  noTone(SPEAKER_PIN);

  Serial.begin(115200);
  //Scanning (SDA : SCL) - GPIO2 : GPIO14 - I2C device found at address 0x68  !
  Wire.begin(2, 14);
  delay(2000);

  pixels.begin();

  while (!mpu.begin()) {
    Serial.println("Failed to find MPU6050 chip");
    delay(500);
  }

  //in case you want to see the clock, but you're too lazy to use the webserver, you can hardcode the time here:
  //setTime(18, 27, 0, 3, 3, 2023);
  StartWebServer();
}

void loop()
{
  if (!CheckSwing() && timeStatus() == timeSet) {
    ShowClock();
  }
  else {
    EffectBasedOnRoll();
  }

  ServeIncomingHTTPConnection();

  interrupts();
  delay(10);
}

unsigned long lastSwing = 0;
uint16_t currentHue = 0;
int swing = 0;

bool CheckSwing() {
  float yaw, pitch, roll = 0;

  /* Get new sensor events with the readings */
  sensors_event_t a, g, temp;
  mpu.getEvent(&a, &g, &temp);

  yaw = g.gyro.x;
  pitch = g.gyro.y;
  roll = g.gyro.z;
  //printRotation(yaw, pitch, roll);

  swing = (int)(abs(roll) + abs(yaw) + abs(pitch)); //absolute values so movement doesn't cancel the effect
  
  if (swing > 0) {
    lastSwing = millis();
  }
  else if ((millis() - lastSwing) > SWINGTIME_MILLIS)
  { 
    //if the last swing time was longer than SWINGTIME_MILLIS ago, we are no longer swinging and can go back to clock face
    return false;
  }
  
  return true;
}

void EffectBasedOnRoll()
{
  currentHue += SLOW_SHIFT + swing * SWING_SHIFT;
  
  // LEDS
  for (int i = 0; i < NUM_LEDS; i++)
  {
    pixels.setPixelColor(i, pixels.ColorHSV(currentHue, 255, 100 + random(-swing*50, swing*50)));
  }
  pixels.show();
}

void printRotation( int yaw, int pitch, int roll)
{
  Serial.print("Yaw:");
  Serial.print(yaw);
  Serial.print(",");
  Serial.print("Pitch:");
  Serial.print(pitch);
  Serial.print(",");
  Serial.print("Roll:");
  Serial.print(roll);
  Serial.print(",");
  Serial.print("interrupts:");
  Serial.print(mpu.getMotionInterruptStatus());
  Serial.println();
}

int lastHour = -1;

void ShowClock()
{
  byte r;
  for (int i = 0; i < NUM_LEDS; i++)
  {
//    byte r = i == ((hour()%12) * 2)? 150: 0;
    if (minute()>30) {
      r = i == ((hour()%12) * 2 + 1)? 150: 0;  
    }
    else {
      r = i == ((hour()%12) * 2)? 150: 0;  
    }
        
    byte g = i == (minute() * 2 / 5)? 150: 0;
    byte b = (second() % 2 == 0 && i == (second() * 2 / 5)) ? 50: 0; //only show blue on even seconds (so we have a blinking light)
    
    pixels.setPixelColor(i, pixels.Color(r, g, b));
  }
  pixels.show();
  if (lastHour != hour()) {
    for (int c=0; c<(hour()>12?hour()-12:hour()); c++) {
      cuckoo();
      delay(500);
    }
  }
  lastHour = hour();
}

void cuckoo()
{
  tone(SPEAKER_PIN, 1400);
  delay(100);
  noTone(SPEAKER_PIN);
  delay(100);
  tone(SPEAKER_PIN, 1000);
  delay(150);
  noTone(SPEAKER_PIN);

}
