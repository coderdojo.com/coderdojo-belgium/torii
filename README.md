# torii

## Getting started

Congratz, You’re a proud owner of the cool Coderdojo Trophy!

## Tutorials

- [🇧🇪🇳🇱 CDJ Trofee: De klok juist zetten](Tutorials/cdj_trophy_de_klok_juist_zetten.md)
- [🇺🇸🇬🇧 CDJ Trophy: Adjusting the clock](Tutorials/cdj_trophy_adjust_clock.md)